"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "articles",
      [
        {
          title:
            "Luhut: Terjadi Kenaikan Angka Kematian di Beberapa Daerah, Perlu Diwaspadai",
          subtitle:
            "Luhut Binsar Pandjaitan mengatakan, saat ini terjadi kenaikan kasus kematian di sejumlah daerah di Jawa Tengah yang perlu diwaspadai.",
          content:
            'JAKARTA, KOMPAS.com - Menteri Koordinator Bidang Kemaritiman dan Investasi (Menko Marves) Luhut Binsar Pandjaitan mengatakan, saat ini terjadi kenaikan kasus kematian di sejumlah daerah di Jawa Tengah.  "Perlu kewaspadaan kita semua sekali lagi, karena terdapat peningkatan kasus konfirmasi atau angka kematian dibeberapa wilayah di Jawa Tengah seperti Kabupaten Sukoharjo, Kabupaten Tegal, dan Kabupaten Semarang," ujar Luhut dalam konferensi pers secara virtual pada Senin (13/9/2021). "Ini early warning juga buat kita, sebab pada sisi lain turun, tapi ada juga kasus yang kelihatan meningkat. Jadi ini harus hati-hati. Jangan kita kembali nanti jadi seperti sebelum tanggal 15 Juli 2021 (lonjakan kasus Covid-19)," kata dia',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: "Tambah 14 Kasus Covid-19 di Tangsel, 58 Pasien Sembuh",
          subtitle:
            "Adapun total kasus Covid-19 di Tangsel berjumlah 30.657 kasus",
          content:
            "TANGERANG SELATAN, KOMPAS.com - Kasus Covid-19 di wilayah Tangerang Selatan (Tangsel) masih terus bertambah. Dinas Kesehatan mencatat, ada penambahan 14 kasus Covid-19 pada Senin (13/9/2021). Dengan demikian, total kasus Covid-19 di Tangsel sampai Senin berjumlah 30.657 kasus. Baca juga: Perpanjangan PPKM Level 3 di Jakarta, Bioskop Diizinkan Buka Satgas Penanganan Covid-19 mengonfirmasi, 29.593 orang di antaranya sudah sembuh, bertambah 58 orang dari data pada Minggu (12/9/2021). Sementara itu, pasien terkonfirmasi positif Covid-19 yang dilaporkan meninggal dunia sebanyak 728 orang. Saat ini, pasien positif Covid-19 yang masih menjalani perawatan berkurang menjadi 336 orang. Para pasien menjalani isolasi mandiri maupun dirawat di rumah sakit rujukan. Dapatkan informasi, inspirasi dan insight di email kamu. Daftarkan email Baca juga: PPKM Diperpanjang Lagi, Jakarta Masih Berstatus Level 3 Kecamatan Pondok Aren masih menjadi wilayah dengan jumlah kasus Covid-19 terbanyak, yakni 6.908 kasus. Berikutnya adalah Kecamatan Pamulang yang mencatatkan 6.723 kasus Covid-19. Tangsel yang sebelumnya berstatus zona oranye Covid-19, kini kembali turun ke zona kuning atau wilayah dengan risiko penularan rendah. Informasi terkait penanganan dan data terkini kasus Covid-19 di Tangsel dapat diakses melalui situs https://lawancovid19.tangerangselatankota.go.id.",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: "Panduan Olahraga di Rumah untuk Orang Tanpa Gejala Covid-19",
          subtitle:
            "WHO berikan panduan olahraga di rumah bagi orang yang karantina mandiri tanpa gejala atau didiagnosis penyakit pernapasan akut.",
          content:
            "KOMPAS.com - Organisasi Kesehatan Dunia (WHO) memberikan panduan olahraga di rumah yang ditujukan untuk orang-orang yang melakukan karantina mandiri tanpa gejala atau didiagnosis penyakit pernapasan akut. Dilansir dari situs resmi WHO, tindakan aktif ini tidak dapat menggantikan panduan medis jika terjadi kondisi kesehatan apa pun. Tinggal di rumah untuk waktu yang lama dapat menimbulkan tantangan yang signifikan untuk tetap aktif secara fisik. Selain itu, jarang melakukan aktivitas fisik dapat memiliki efek negatif pada kesehatan, kesejahteraan dan kualitas hidup individu. Oleh karena itu, WHO merekomendasikan masyarakat untuk tetap beraktivitas fisik dengan intensitas sedang atau 75 menit aktivitas kuat per minggu",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title:
            "Tak Ingin Kecolongan Ada Klaster Baru, Wabup Sumedang Datangi Pabrik di Kawasan Industri",
          subtitle:
            "Wabup Erwan menyebutkan, dalam upaya penanganan Covid-19, Pemkab Sumedang juga terus melakukan upaya percepatan vaksinasi.",
          content:
            'SUMEDANG, KOMPAS.com - Wakil Bupati Sumedang Erwan Setiawan mengunjungi sejumlah pabrik di kawasan industri di Kecamatan Cimanggung dan Kecamatan Jatinangor, Kabupaten Sumedang, Jawa Barat, Senin (13/9/2021). Erwan mengatakan, kunjungannya ini untuk memastikan perusahaan di kawasan industri di Sumedang menjalankan protokol kesehatan secara ketat. "Kami tidak ingin kecolongan dengan kembali melonjaknya kasus Covid-19 di wilayah Sumedang. Untuk itu, saya datang sendiri untuk memastikan perusahaan-perusahaan di Sumedang ini patuh dalam menjalankan protokol kesehatan," ujar Erwan kepada Kompas.com di PT Yakjin Jaya Indonesia, Senin. Baca juga: Lapas Sumedang Kekurangan APAR, Karung Pasir hingga Lap Basah Disiapkan untuk Cegah Kebakaran Erwan menuturkan, dari beberapa perusahaan di kawasan industri yang dikunjungi ini, ia mengapresiasi terobosan yang dilakukan oleh PT Yakjin Jaya Indonesia, karena menyediakan ruang isolasi khusus bagi buruh di perusahaannya. "Di perusahaan ini, prosedur protokol kesehatannya sangat lengkap, bahkan ada juga ruang isolasi bagi buruh. Sehingga bila ada buruh yang terindikasi sakit akibat Covid-19 bisa ditangani secara cepat," tutur Erwan. Erwan menyebutkan, dalam upaya penanganan Covid-19, Pemkab Sumedang juga terus melakukan upaya percepatan vaksinasi',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("articles", null, {});
  },
};
