const express = require("express");
const router = express.Router();
const article = require("../controllers/articleController");

router.get("/api/article", article.getArticle);
router.post("/api/article", article.addArticle);
router.get("/api/article/:id", article.getArticleById);
router.put("/api/article/:id", article.updateArticle);
router.delete("/api/article/:id", article.deleteArticle);

module.exports = router;
