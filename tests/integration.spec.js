const request = require("supertest");
const app = require("../app.js");

describe("User API", () => {
  it("should show all articles", async () => {
    const res = await request(app).get("/api/article");
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty("data");
  });

  it("should show an article", async () => {
    const res = await request(app).get("/api/article/2");
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty("article");
  }),
    it("should create a new article", async () => {
      const res = await request(app).post("/api/article").send({
        title: "New Title",
        subtitle: "New SubTitle",
        content: "New Content",
      });
      expect(res.statusCode).toEqual(201);
      expect(res.body).toHaveProperty("newArticle");
    }),
    it("should update an article", async () => {
      const res = await request(app).put("/api/article/5").send({
        title: "Update Title",
        subtitle: "Update SubTitle",
        content: "Update Content",
      });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty("article");
    }),
    it("should delete an article", async () => {
      const res = await request(app).del("/api/article/5");
      expect(res.statusCode).toEqual(204);
    });
});
