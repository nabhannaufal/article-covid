const { articles } = require("../models");

module.exports = {
  getArticle: async (req, res) => {
    try {
      const data = await articles.findAll();
      return res.status(200).json({ data });
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  },

  addArticle: async (req, res) => {
    try {
      const newArticle = await articles.create(req.body);
      return res.status(201).json({
        newArticle,
      });
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  },

  updateArticle: async (req, res) => {
    try {
      const { id } = req.params;
      const [updated] = await articles.update(req.body, {
        where: { id: id },
      });
      if (updated) {
        const updatedArticle = await articles.findOne({ where: { id: id } });
        return res.status(200).json({ article: updatedArticle });
      }
      throw new Error("Article not found");
    } catch (error) {
      return res.status(500).send(error.message);
    }
  },

  getArticleById: async (req, res) => {
    try {
      const { id } = req.params;
      const article = await articles.findOne({
        where: { id: id },
      });
      if (article) {
        return res.status(200).json({ article });
      }
      return res
        .status(404)
        .send("Article with the specified ID does not exists");
    } catch (error) {
      return res.status(500).send(error.message);
    }
  },

  deleteArticle: async (req, res) => {
    try {
      const { id } = req.params;
      const deleted = await articles.destroy({
        where: { id: id },
      });
      if (deleted) {
        return res.status(204).send("Article deleted");
      }
      throw new Error("article not found");
    } catch (error) {
      return res.status(500).send(error.message);
    }
  },
};
